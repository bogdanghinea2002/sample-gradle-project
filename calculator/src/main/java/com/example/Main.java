package com.example;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String input = scanner.nextLine();
        scanner.close();
        Calculator calculator = new Calculator(input);
        System.out.println(calculator.calculate());
    }
}