package com.example;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class Calculator {
    private List<String> atoms;
    private int index;

    public Calculator(String text) {
        AtomsGenerator atomsGenerator = new AtomsGenerator();
        atoms = atomsGenerator.generateAtoms(text);
        index = 0;
    }

    public double calculate() {
        double result = 0;
        while (index < atoms.size()) {
            List<String> factors = getNextFactors();
            result += calculateFactor(factors);
        }
        return result;
    }

    private double calculateFactor(List<String> factors) {
        double result = Double.parseDouble(factors.get(0));
        int i = 1;
        while (i < factors.size()) {
            if (Objects.equals(factors.get(i), "*")) {
                result *= Double.parseDouble(factors.get(i + 1));
            } else {
                result /= Double.parseDouble(factors.get(i + 1));
            }
            i += 2;
        }
        return result;
    }

    private List<String> getNextFactors() {
        List<String> factors = new ArrayList<>();
        while (index < atoms.size() && !Objects.equals(atoms.get(index), "+")) {
            factors.add(atoms.get(index));
            index++;
        }
        index++;
        return factors;
    }
}
