package com.example;

import java.util.ArrayList;
import java.util.List;

public class AtomsGenerator {

    public List<String> generateAtoms(String text) {
        List<String> atoms = new ArrayList<>();
        atoms.add("0");
        atoms.add("+");
        int i = 0;
        while (i < text.length()) {
            char current = text.charAt((i));
            if (!Character.isDigit(current) && current != '-') { // current -> + | * | /
                atoms.add(String.valueOf(current));
                i++;
            }
            if (i >= text.length()) {
                break;
            }
            String nextNumber = getNextNumber(text, i);
            atoms.add(nextNumber);
            i += nextNumber.length();
        }
        addPluses(atoms);
        return atoms;
    }
    private void addPluses(List<String> atoms){
        int i=1;
        while(i<atoms.size()){
            if(isNumber(atoms.get(i-1)) && isNumber(atoms.get(i))){
                atoms.add(i,"+");
            }
            i++;
        }
    }
    private boolean isNumber(String s){
        return s.charAt(0) == '-' || Character.isDigit(s.charAt(0));
    }
    private String getNextNumber(String text, int i) {
        char current = text.charAt((i));
        StringBuilder number = new StringBuilder();
        if (current == '-') {
            number.append("-");
            i++;
        }

        while (i < text.length() && Character.isDigit(text.charAt((i)))) {
            number.append(text.charAt((i)));
            i++;
        }
        return number.toString();
    }
}
