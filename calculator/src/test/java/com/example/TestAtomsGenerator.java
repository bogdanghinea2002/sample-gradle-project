package com.example;

import org.hamcrest.MatcherAssert;
import org.hamcrest.Matchers;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.hamcrest.Matchers.containsString;

public class TestAtomsGenerator {
    public AtomsGenerator atomsGenerator;
    @BeforeEach
    public void setup() {
        atomsGenerator = new AtomsGenerator();
    }

    @Test
    public void testAtomsSize() {
        List<String> atoms = atomsGenerator.generateAtoms("1+2");
        MatcherAssert.assertThat(atoms.size(),  Matchers.equalTo(5));
    }
    @Test
    public void testAtoms() {
        List<String> atoms = atomsGenerator.generateAtoms("-1/2+3");
        List<String> expectedAtoms = new ArrayList<>(Arrays.asList("0","+","-1","/","2","+","3"));
        MatcherAssert.assertThat(atoms,  Matchers.equalTo(expectedAtoms));
    }
}
