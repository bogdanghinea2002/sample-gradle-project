package com.example;

import org.hamcrest.MatcherAssert;
import org.hamcrest.Matchers;
import org.junit.jupiter.api.Test;


public class TestCalculator {
    @Test
    public void testCalculator1() {
        Calculator calculator = new Calculator("1+2+3");
        MatcherAssert.assertThat(calculator.calculate(),  Matchers.equalTo(6.0));
    }
    @Test
    public void testCalculator2() {
        Calculator calculator = new Calculator("-2/-2*-3-4+7");
        MatcherAssert.assertThat(calculator.calculate(),  Matchers.equalTo(0.0));
    }
}
